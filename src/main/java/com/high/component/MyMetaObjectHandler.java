package com.high.component;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author zhaozhengshi
 * @version 1.0
 * @ClassName: MyMetaObjectHandler
 * @Description:
 * @date: 2022/3/2 11:28
 * @since JDK 1.8
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        // 自动填充有这个属性才填充
        boolean hasSetter = metaObject.hasSetter("createTime");
        if(hasSetter){
            System.out.println("insertFill~~");
            setInsertFieldValByName("createTime", LocalDateTime.now(),metaObject);
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        // 更新时如果属性未设置值时才进行自动填充
        Object val = getFieldValByName("updateTime", metaObject);
        if(null == val){
            System.out.println("updateFill~~");
            setUpdateFieldValByName("updateTime", LocalDateTime.now(),metaObject);
        }
    }
}
