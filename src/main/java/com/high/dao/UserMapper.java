package com.high.dao;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.high.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author zhaozhengshi
 * @version 1.0
 * @ClassName: UserMapper
 * @Description:
 * @date: 2022/3/02 16:02
 * @since JDK 1.8
 */
public interface UserMapper extends MyMapper<User> {

    @SqlParser(filter = true)// 多租户，不添加租户信息
    @Select("select * from user ${ew.customSqlSegment}")
    List<User> mySelectList(@Param(Constants.WRAPPER)Wrapper<User> wrapper);

}
