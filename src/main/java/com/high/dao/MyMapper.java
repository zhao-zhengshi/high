package com.high.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhaozhengshi
 * @version 1.0
 * @ClassName: MyMapper
 * @Description: 3.在mapper中加入自定义方法
 * @date: 2022/3/2 17:26
 * @since JDK 1.8
 */
public interface MyMapper<T> extends BaseMapper<T> {

    /**
     * @author zhaozhengshi
     * @Description  删除所有
     * @Date 2022/3/2 17:12
     * @Param
     * @return int 影响行数
     */
    int deleteAll();

    /**
     * @author zhaozhengshi
     * @Description 新增时自选字段
     * @Date 2022/3/3 10:08
     * @Param [list]
     * @return int
     */
    int insertBatchSomeColumn(List<T> list);

    /**
     * @author zhaozhengshi
     * @Description 根据id逻辑删除数据，并带字段填充功能
     * @Date 2022/3/3 10:30
     * @Param [entity]
     * @return int
     */
    int deleteByIdWithFill(T entity);

    /**
     * @author zhaozhengshi
     * @Description 根据id更新固定的某些字段
     * @Date 2022/3/3 10:45
     * @Param [entity]
     * @return int
     */
    int alwaysUpdateSomeColumnById(@Param(Constants.ENTITY)T entity);
}
