package com.high.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * @author zhaozhengshi
 * @version 1.0
 * @ClassName: User
 * @Description:
 * @date: 2022/3/02 15:57
 * @since JDK 1.8
 */
@Data
public class User{

    private Long id;
    //姓名
    private String name;
    //年龄
    @TableField(fill = FieldFill.UPDATE)
    private Integer age;
    //邮箱
    private String email;
    //直属上级
    private Long managerId;
    //创建时间
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    //修改时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    //版本
    @Version
    private Integer version;
    // 逻辑删除标识（0删除 1已删除）
    //    TableField查询排除删除标识字段
    @TableLogic
    @TableField(select = false)
    private Integer deleted;

}
