package com.high.method;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

/**
 * @author zhaozhengshi
 * @version 1.0
 * @ClassName: DeleteAllMethod
 * @Description: 1.创建自定义方法
 * @date: 2022/3/2 17:04
 * @since JDK 1.8
 */
public class DeleteAllMethod extends AbstractMethod {

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
//        执行的sql
        String sql = "delete from " + tableInfo.getTableName();
//        mapper接口方法名
        String method = "deleteAll";
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);

        return addDeleteMappedStatement(mapperClass,method,sqlSource);
    }
}
