package com.high;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.high.config.MybatisPlusConfiguration;
import com.high.dao.UserMapper;
import com.high.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
class InjectorTest {

    @Autowired(required = false)
    private UserMapper userMapper;

    /*
    sql注入器使用
    1.创建自定义方法
    2.创建注入器
    3.在mapper中加入自定义方法
     */
    @Test
    public void deleteAll() {
        int rows = userMapper.deleteAll();
        System.out.println("影响行数："+rows);
    }

    /*
    选装件
    新增时自选字段
     */
    @Test
    public void insertBatch() {
        User user1 = new User();
        user1.setName("李兴华2");
        user1.setAge(34);
        user1.setManagerId(1088248166370832385L);

        User user2 = new User();
        user2.setName("杨红2");
        user2.setAge(29);
        user2.setManagerId(1088248166370832385L);

        List<User> userList = Arrays.asList(user1, user2);
        int rows = userMapper.insertBatchSomeColumn(userList);
        System.out.println(rows);
    }

    /*
    选装件
    根据id逻辑删除数据，并带字段填充功能
     */
    @Test
    public void deleteByIdWithFill() {
        User user = new User();
        user.setId(1499207359443898370L);
        user.setAge(35);

        int rows = userMapper.deleteByIdWithFill(user);
        System.out.println(rows);
    }

    /*
    选装件
    根据id更新固定的某些字段
     */
    @Test
    public void alwaysUpdateSomeColumnById() {
        User user = new User();
        user.setId(1088248166370832385L);
        user.setAge(26);
        user.setName("王第风");

        int rows = userMapper.alwaysUpdateSomeColumnById(user);
        System.out.println(rows);
    }

}
