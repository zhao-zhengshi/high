package com.high;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.high.dao.UserMapper;
import com.high.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest
class FillTest {

    @Autowired(required = false)
    private UserMapper userMapper;

    /*
    插入数据，创建时间自动填充
     */
    @Test
    public void insert() {
        User user = new User();
        user.setName("刘明超");
        user.setAge(31);
        user.setEmail("lmc@baomidou,com");
        user.setManagerId(1087982257332887553L);
        int rows = userMapper.insert(user);
        System.out.println("影响行数："+rows);
    }


    /*
    更新逻辑未删除数据
     */
    @Test
    public void updateById() {
        User user = new User();
        user.setAge(27);
        user.setId(1088248166370832385L);
        user.setUpdateTime(LocalDateTime.now());
        int rows = userMapper.updateById(user);
        System.out.println("影响行数："+rows);
    }


}
