package com.high;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.high.dao.UserMapper;
import com.high.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

@SpringBootTest
class OptTest {

    @Autowired(required = false)
    private UserMapper userMapper;

    /*
    乐观锁注意事项
    Wrapper千万别复用
     */
    @Test
    public void update() {
        int version = 2;

        User user = new User();
        user.setEmail("lmc2@baomidou,com");
        user.setVersion(version);
        QueryWrapper<User> query = Wrappers.<User>query();
        query.eq("name", "刘明超");

        int rows = userMapper.update(user, query);
        System.out.println("影响行数：" + rows);

        //        复用Wrapper
        int version2 = 2;

        User user2 = new User();
        user2.setEmail("lmc2@baomidou,com");
        user2.setVersion(version2);
        query.eq("age", 25);

        int rows2 = userMapper.update(user2, query);
        System.out.println("影响行数：" + rows2);
    }


    /*
    乐观锁实现，更新数据
     */
    @Test
    public void updateById() {
        // 比如：查询出来的版本为1
        int version = 1;

        User user = new User();
        user.setEmail("lmc2@baomidou,com");
        user.setId(1498865625446981634L);
        user.setVersion(version);
        int rows = userMapper.updateById(user);
        System.out.println("影响行数：" + rows);
    }


}
