package com.high;

import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.high.config.MybatisPlusConfiguration;
import com.high.dao.UserMapper;
import com.high.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class MyTest {

    @Autowired(required = false)
    private UserMapper userMapper;

    /*
    实现逻辑删除
     */
    @Test
    public void deleteById() {
        int rows = userMapper.deleteById(1094592041087729666L);
        System.out.println("影响行数："+rows);
    }

    /*
    逻辑删除后查询
     */
    @Test
    public void select() {
        List<User> list = userMapper.selectList(null);
        list.forEach(System.out::println);
    }

    /*
    更新逻辑未删除数据
     */
    @Test
    public void updateById() {
        User user = new User();
        user.setAge(26);
        user.setId(1088248166370832385L);
        int rows = userMapper.updateById(user);
        System.out.println("影响行数："+rows);
    }

    /*
    自定义查询，排除删除的数据
     */
    @Test
    public void mySelect() {
        //        动态表名不会生效，@SqlParser(filter = true)会过滤掉动态表名
        MybatisPlusConfiguration.myTableName.set("user_2019");
        List<User> list = userMapper.mySelectList(Wrappers.<User>lambdaQuery()
                .gt(User::getAge,25).eq(User::getDeleted,0));
        list.forEach(System.out::println);
    }

    /*
    特定sql过滤
     */
    @Test
    public void seleteById() {
//        动态表名不会生效，特定sql过滤器会过滤掉动态表名
        MybatisPlusConfiguration.myTableName.set("user_2019");
        User user = userMapper.selectById(1088248166370832385L);
        System.out.println(user);
    }

    /*
    动态表名的查询
     */
    @Test
    public void select2() {
        MybatisPlusConfiguration.myTableName.set("user_2019");
        List<User> list = userMapper.selectList(null);
        list.forEach(System.out::println);
    }


}
